import mongoose from "mongoose"

export const connectToTheDatabase = (MONGODB_URI: string | undefined) => {
  if (MONGODB_URI) {
    mongoose.connect(MONGODB_URI, {}).then((con: typeof mongoose) => {
      const { host, name, port } = con.connection
      console.log(`🏬 [Database] : Database is Connected with Host:${host}\n✨ [Database Name] : ${name}\n🧆 [Database Port] : ${port}`)
    }).catch(e => {
      console.log("Error in Connecting Database")
    })
  } else {
    throw new Error("Connection URL String is not provided")
  }
}
