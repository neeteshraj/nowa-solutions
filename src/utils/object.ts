export const checkKeysInObject = (obj: any, stringToCheck: string) => {
	if (
		Object.keys(obj).some(function (k) {
			return ~k.indexOf(stringToCheck);
		})
	) {
		return true;
	} else {
		return false;
	}
};
// getSpecificKeysInObject(checkData.data, 'purchase_info');
export const getSpecificKeysInObject = (obj: any, stringToCheck: string) => {
	const result: any = {};
	for (const [key, value] of Object.entries(obj)) {
		console.log(`${key}: ${value}`);
		if (key.includes(stringToCheck)) {
			const words: string[] = key.split('.');
			result[words[1]] = value;
		}
	}
	return result;
};
