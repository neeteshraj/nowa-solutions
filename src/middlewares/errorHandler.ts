import { Express, NextFunction, Request, Response } from 'express';
export const errorHandler = (app: Express) => {

  // Error Handler Middleware
  app.use(function (err: any, req: Request, res: Response, next: NextFunction) {
    console.log(err)
    if (err.name === 'CastError') {
      err._message = `Resource Not Found. Invalid: ${err.path}`
    }
    if (err.code === 11000) {
      err._message = 'Duplication Error'
    }
    if (err.name === 'ValidationError') {
      err._message = Object.values(err.errors).map((value: any) => value.message)
      err.path = Object.values(err.errors).map((value: any) => value.path)
    }
    if (process.env.NODE_ENV === 'development') {
      console.log("ERRR>>", err._message)
      res.status(err.status || 500).json({
        success: false,
        status: err.status || 500,
        code: err.code,
        message: err._message || err.message || 'Error from error handling middleware',
        path: err.path,
        errorPlace: err.keyPattern,
        errorData: err.keyValue,
        stack: err.stack
      });
    }
    if (process.env.NODE_ENV === 'production') {
      res.status(err.status || 500).json({
        success: false,
        status: err.status || 500,
        message: err._message || err.message || 'Error from error handling middleware',
      });
    }

  });
}
