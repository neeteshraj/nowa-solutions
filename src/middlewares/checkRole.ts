import { commonConstants } from './../common/constant';
import { Request, Response, NextFunction } from 'express';
import { handleErrorResponse } from '../common/response';

export const authorizeRole = async (req: Request, res: Response, next: NextFunction) => {
	// const role = req.user.involvement.role
	// if (role !== commonConstants.roles.SUPER_ADMIN && role !== commonConstants.roles.ADMIN)
	//   return handleErrorResponse(res, 401, { message: "Please Contact to your superior for access." })

	next();
};
