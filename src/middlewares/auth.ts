import { verify } from 'jsonwebtoken';
import { handleErrorResponse } from '../common/response';

export const authenticateToken = async (req: Request | any, res: any, next: any) => {
	const authHeader = req.headers['authorization'];
	const token = authHeader && authHeader.split(' ')[1];

	if (token == null) return handleErrorResponse(res, 401, { message: 'UnAuthorized' });
	verify(token, process.env.ACCESS_TOKEN_SECRET as string, (err: any, user: any) => {
		if (err) {
			// console.log("ERROR TOKEN", err)
			if (err.name === 'TokenExpiredError') {
				return handleErrorResponse(res, 403, { message: 'Token Has Expired' });
			}
			if (err.name === 'JsonWebTokenError') {
				return handleErrorResponse(res, 403, { message: 'Token Has Altered/Expired' });
			}
			return handleErrorResponse(res, 403, {});
		}
		req.user = user;
		// console.log("USER AUTH", user)
		next();
	});
};
