import dotenv from 'dotenv';
import express, { Express, Request, Response } from 'express';
import { errorHandler } from './middlewares/errorHandler';
import { routes } from './routes';
import { connectToTheDatabase } from './db/connection';
import logger from 'morgan';
import cors from 'cors';

//Configure environment variables
dotenv.config();

//initializing application
const app: Express = express();

//Middlewares
app.use(logger('dev'));
app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: false }));

//Define Constants
const port = process.env.PORT;
const db = process.env.MONGODB_URI ?? process.env.LOCAL_URL;
const host_name = process.env.HOST_NAME;
//Connection to Database
connectToTheDatabase(db);

//Define Routes
app.get('/', (req: Request, res: Response) => {
	res.send('Express + TypeScript Server');
});

routes(app);

//Error Handler
errorHandler(app);

//Whole Application listener
app.listen(port, () => {
	console.log(`⚡️ [server]: Server is running at ${host_name}:${port}`);
});
