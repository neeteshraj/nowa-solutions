import { Express } from 'express';
import authRouter from './module/auth/router';
import userRoute from './module/user/router';
import externalAccessRouter from './module/externalAccess/router';
import company from './module/company/router';

export const routes = (app: Express): void => {
	try {
		app.use('/api/auth', authRouter);
		app.use('/api/user', userRoute);
		app.use('/api/externalAccess', externalAccessRouter);
		app.use('/api/company', company);
	} catch (err) {
		console.log('Error initializing routes:', err);
	}
};
