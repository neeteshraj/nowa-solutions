export const populateConstants = {
	company: {
		path: 'company',
		model: 'companies',
		select: '_id name type owner createdAt',
	},
};
