export const SUPER_ADMIN = {
  "name": "super admin",
  "permission": [
    {
      "name": "user management",
      "actions": [
        { "name": "Create" },
        { "name": "Update" },
        { "name": "Delete" },
        { "name": "List" }
      ]
    }, {
      "name": "company management",
      "actions": [
        { "name": "Create" },
        { "name": "Update" },
        { "name": "Delete" },
        { "name": "List" }
      ]
    }
  ]
}
export const ADMIN = {
  "name": "admin",
  "permission": [
    {
      "name": "user management",
      "actions": [
        { "name": "Create" },
        { "name": "Update" },
        { "name": "Delete" },
        { "name": "List" }
      ]
    }, {
      "name": "company management",
      "actions": [
        { "name": "Create" },
        { "name": "Update" },
        { "name": "Delete" },
        { "name": "List" }
      ]
    }
  ]
}
export const CUSTOMER = {
  "name": "customer",
  "permission": [
    {
      "name": "user management",
      "actions": [
        { "name": "Create" },
        { "name": "Update" },
        { "name": "Delete" },
        { "name": "List" }
      ]
    }, {
      "name": "company management",
      "actions": [
        { "name": "Create" },
        { "name": "Update" },
        { "name": "Delete" },
        { "name": "List" }
      ]
    }
  ]
}
export const MEMBER = {
  "name": "member",
  "permission": [
    {
      "name": "user management",
      "actions": [
        { "name": "Update" },
        { "name": "List" }
      ]
    }, {
      "name": "company management",
      "actions": [
        { "name": "List" }
      ]
    }
  ]
}