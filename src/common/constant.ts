export const commonConstants = {
	permission: {
		ACTIONS: 'ACTIONS',
		PERMISSION: 'PERMISSION',
		NEW_ACTION: 'NEW_ACTION',
		NEW_PERMISSION: 'NEW_PERMISSION',
	},
	messages: {
		errors: {
			LISTING_ERROR: 'Listing Error',
			INVALID_QUERY: 'Invalid Query',
		},
	},
	roles: {
		SUPER_ADMIN: 'SUPER_ADMIN',
		ADMIN: 'ADMIN',
		CUSTOMER: 'CUSTOMER',
		SUPPLIER: 'SUPPLIER',
	},

	RETAIL: 'RETAIL',
	VENDOR: 'VENDOR',
	Company: 'Company',
	actions: {
		ADD: 'ADD',
		UPDATE: 'UPDATE',
	},
	type: {
		PAID: 'PAID',
		['UN PAID']: 'UN PAID',
	},
	status: {
		DELIVERED: 'DELIVERED',
		'IN PROGRESS': 'IN PROGRESS',
		'ON HOLD': 'ON HOLD',
		REJECTED: 'REJECTED',
	},
};
