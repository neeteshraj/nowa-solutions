import { Response } from "express";

export const handleResponse = (res: Response, status: number, data: any) => {
  res.status(status).json({ ...data, success: true });
}
export const handleErrorResponse = (res: Response, status: number, data: any) => {
  res.status(status).json({ ...data, success: false });
}