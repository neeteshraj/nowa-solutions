import nodemailer from 'nodemailer';
import SMTPTransport from 'nodemailer/lib/smtp-transport';

const sendEmail = async (to: string, subject: string, html: string) => {
	const credentials: SMTPTransport | SMTPTransport.Options = {
		host: process.env.SMTP_HOST,
		port: Number(process.env.SMTP_PORT),
		auth: {
			user: process.env.SMTP_USER,
			pass: process.env.SMTP_PASS,
		},
	};
	const transport = nodemailer.createTransport(credentials);
	subject = `${subject}`;
	const data = {
		from: process.env.MAIL_FROM,
		to,
		subject,
		html,
	};
	await transport.sendMail(data, function (error: Error | null, info: SMTPTransport.SentMessageInfo) {
		if (error) {
			console.log(error);
		} else {
			console.log('Email sent: ' + info.response);
		}
	});
};

export default sendEmail;
