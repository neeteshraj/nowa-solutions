import nodemailer from 'nodemailer';
import SMTPTransport from 'nodemailer/lib/smtp-transport';

const sendEmail = (to: string, subject: string, html: string) => {
	subject = `${subject}`;
	const data = {
		from: process.env.MAIL_FROM,
		to,
		subject,
		html,
	};
	const transport = nodemailer.createTransport({
		service: process.env.MAIL_SERVICE,
		auth: {
			user: process.env.MAIL_USER,
			pass: process.env.MAIL_PASSWORD,
		},
		debug: true,
		logger: true,
	});
	transport.sendMail(data, function (error: Error | null, info: SMTPTransport.SentMessageInfo) {
		if (error) {
			console.log('ERROR', error);
		} else {
			console.log('Email sent: ' + info.response);
		}
	});
};

export default sendEmail;
