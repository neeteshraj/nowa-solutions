import { Capitalize } from '../../utils/capitalize';
import { IUser } from './interface';

export const mapUserData = (data: IUser) => {
	if (data.name) data.name = Capitalize(data.name);
	return data;
};

export const userProjection = (data: IUser) => {
	const res = {
		_id: data._id,
		name: data.name,
		username: data.username,
		email: data.email,
		active: data.active,
		createdAt: data.createdAt,
	};
	return res;
};
