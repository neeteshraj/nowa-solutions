import mongoose from 'mongoose';
import { populateConstants } from '../../common/populate';
import { commonConstants } from './../../common/constant';
import { userConstants } from './constant';
import { IUser } from './interface';
import UserModel from './model';

//CREATE USER
export const create = async (user: IUser): Promise<IUser> => await UserModel.create(user);

//FETCH LIST OF USERS
export const list = async (condition: any, limit: number, skip: number): Promise<{ totalCount: number; data: IUser[] }> => {
	return new Promise((resolve, reject) => {
		UserModel.aggregate([
			{
				$facet: {
					data: [
						{ $match: { ...condition, isDeleted: false } },
						{ $sort: { name: 1 } },
						{ $limit: limit ?? 20 },
						{ $skip: skip ?? 0 },
						{ $lookup: { from: 'companies', localField: 'currentInvolvement.company', foreignField: '_id', as: 'currentInvolvement.company_details' } },
						{ $project: { password: 0 } },
					],
					total: [{ $match: { ...condition, isDeleted: false } }, { $count: 'totalCount' }],
				},
			},
		])
			.then(([{ data, total }]) => {
				let totalCount: number;
				total.length > 0 ? (totalCount = total[0].totalCount) : (totalCount = 0);
				return resolve({ totalCount, data });
			})
			.catch((e) => reject({ msg: commonConstants.messages.errors.LISTING_ERROR }));
	});
};

//FETCH THE SPECIFIC USER ACC TO _id
export const findOne = async (condition: any): Promise<IUser | null> => await UserModel.findOne({ ...condition, active: true, isDeleted: false });

export const findOneWithPopulate = async (condition: any): Promise<IUser | null> =>
	await UserModel.findOne({ ...condition, active: true, isDeleted: false }).populate([
		{
			path: 'currentInvolvement',
			populate: populateConstants.company,
		},
		{
			path: 'involvement',
			populate: populateConstants.company,
		},
	]);

//UPDATE USER
export const update = async (condition: any, data: any, arrayFilters?: any): Promise<any | null> => await UserModel.findOneAndUpdate({ ...condition, isDeleted: false }, data, { arrayFilters, new: true });
// await UserModel.findOneAndUpdate({ ...condition, isDeleted: false }, data, { new: true, projection: userConstants.projection })

// REMOVE USER
export const remove = async (condition: any): Promise<IUser | null> => await UserModel.findOneAndUpdate({ ...condition, isDeleted: false }, { $set: { isDeleted: true } }, { new: true, projection: { _id: 1 } });

//CHECK USER EXISTING IN THE SYSTEM ACC TO EMAIL AND USERNAME
export const checkExistingEmailOrUsername = async (username: string, email: string): Promise<IUser | null> => await UserModel.findOne({ $or: [{ username: username }, { email: email }] });

//ACTIVATE USER
export const activate = async (_id: string): Promise<void> => {
	await UserModel.updateOne({ _id }, { $set: { active: true } });
};

export const findById = async (id: string): Promise<IUser | null> => await UserModel.findById(id, { isDeleted: false });

export const findByEmail = async (email: string): Promise<IUser | null> => await UserModel.findOne({ email: email });

export const search = async (keyword: any, org: mongoose.Types.ObjectId) => {
	const regExp = keyword
		.trim()
		.replace(/\s\s+/g, ' ')
		.split(' ')
		.map((item: any) => new RegExp(item, 'i'));
	const data = await UserModel.aggregate([
		{
			$match: {
				// $and: [{ name: { $in: regExp } }, { 'currentInvolvement.company': org }],
				$and: [{ name: { $in: regExp } }, { 'involvement.company': org }, { active: true }],
			},
		},
	]);
	return data;
	// return await UserModel.find({ name: { $regex: condition, $options: '$i' } });
};
