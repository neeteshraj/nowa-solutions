export interface IInvolvementCompany {
	companyId: string;
	role: string;
}
export interface IAddress {
	street: string;
	city: string;
	state: string;
	zip: string;
	country: string;
}

export interface IUser {
	_id: string;
	name: string;
	username: string;
	email: string;
	password: string;
	address: string;
	phone_number: string;
	gender: string;
	company: IInvolvementCompany;
	active: boolean;
	isDeleted: boolean;
	createdAt: string;
}
