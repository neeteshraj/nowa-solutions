import { commonConstants } from './../../common/constant';
import * as service from './services';
import { userConstants } from './constant';
import { Response, NextFunction } from 'express';
import { handleErrorResponse, handleResponse } from '../../common/response';
import { IUser } from './interface';
import { mapUserData, userProjection } from './helper';

import mongoose from 'mongoose';

export const create = async (req: any, res: Response, next: NextFunction) => {
	try {
		const body = req.body;
		//check the involvement of the authenticated user and assign involvement to new user with role.
		//role must be provided in body.
		const { company, type } = req.user.involvement;
		const involvement = { company, type, role: body.role.toUpperCase() };
		body.involvement = [involvement];
		body.currentInvolvement = involvement;
		const data: IUser = await service.create(body);
		//RESPONSE STATEMENTS
		const response = { message: userConstants.messages.User_Created_Successfully, data: userProjection(data) };
		handleResponse(res, 201, response);
	} catch (e) {
		next(e);
	}
};

export const search = async (req: any, res: Response, next: NextFunction) => {
	const keyword = req.query.keyword;
	//If keyword is not defined then return Invalid query
	if (!keyword) return next(new Error(userConstants.messages.errors.INVALID_QUERY));
	const org = new mongoose.Types.ObjectId(req.user.involvement.company);
	try {
		const data = await service.search(keyword, org);
		handleResponse(res, 201, { data });
	} catch (e) {
		next(e);
	}
};

export const fetchUsers = async (req: any, res: Response, next: NextFunction) => {
	const offset = req.query.offset;
	const limit = req.query.limit;
	const inactive = !!req.query.inactive;
	try {
		const condition = {
			//if inactive is false then it should return all active data only.
			...(!inactive && { active: true }),
			'currentInvolvement.company': new mongoose.Types.ObjectId(req.user.involvement.company),
		};
		const response = await service.list(condition, Number(limit ?? 20), Number(offset ?? 0));
		handleResponse(res, 201, response);
	} catch (e) {
		next(e);
	}
};

export const fetchUser = async (req: any, res: Response, next: NextFunction) => {
	try {
		const data: IUser | null = await service.findOneWithPopulate({ _id: req.params.id, 'currentInvolvement.company': req.user.involvement.company });
		if (!data) throw new Error(userConstants.messages.User_Not_Found);
		//RESPONSE STATEMENTS
		// const response = { data: userProjection(data) }
		const response = { data };
		handleResponse(res, 200, response);
	} catch (e) {
		next(e);
	}
};

export const update = async (req: any, res: Response, next: NextFunction) => {
	try {
		const body = mapUserData(req.body);
		const data: IUser | null = await service.update({ _id: req.params.id, 'currentInvolvement.company': req.user.involvement.company }, body);
		if (!data) throw new Error(userConstants.messages.User_Not_Found);
		//RESPONSE STATEMENTS
		const response = { message: userConstants.messages.User_Updated_Successfully, data };
		handleResponse(res, 200, response);
	} catch (e) {
		next(e);
	}
};

// {
//     "type":"add",
//     "shipping_address":{
//         "city":"ktm",
//         "zip":"44600",
//         "country":"nepal",
//         "street":"gaurighat",
//         "state":"ktm"
//     }
// }

// {
//   "type":"update",
//   "_id":"6280eb88f9161479793763ab",
//   "shipping_address":{
//       "state":"bagmati"
//   }
// }

export const updateAddress = async (req: any, res: Response, next: NextFunction) => {
	try {
		const { type, shipping_address, mailing_address, _id } = req.body;
		let body;
		let arrayFilters;
		if (type.toUpperCase() === commonConstants.actions.ADD) {
			body = { $push: { ...(shipping_address ? { shipping_address: shipping_address } : { mailing_address }) } };
		} else if (type.toUpperCase() === commonConstants.actions.UPDATE) {
			if (shipping_address) {
				body = {
					$set: {
						...(shipping_address.street && { 'shipping_address.$[s].street': shipping_address.street }),
						...(shipping_address.city && { 'shipping_address.$[s].city': shipping_address.city }),
						...(shipping_address.state && { 'shipping_address.$[s].state': shipping_address.state }),
						...(shipping_address.zip && { 'shipping_address.$[s].zip': shipping_address.zip }),
						...(shipping_address.country && { 'shipping_address.$[s].country': shipping_address.country }),
						...(shipping_address.isDeleted && { 'shipping_address.$[s].isDeleted': shipping_address.isDeleted }),
					},
				};
				arrayFilters = [{ 's._id': _id }];
			} else {
				body = {
					$set: {
						...(mailing_address.street && { 'mailing_address.$[s].street': mailing_address.street }),
						...(mailing_address.city && { 'mailing_address.$[s].city': mailing_address.city }),
						...(mailing_address.state && { 'mailing_address.$[s].state': mailing_address.state }),
						...(mailing_address.zip && { 'mailing_address.$[s].zip': mailing_address.zip }),
						...(mailing_address.country && { 'mailing_address.$[s].country': mailing_address.country }),
						...(mailing_address.isDeleted && { 'mailing_address.$[s].isDeleted': mailing_address.isDeleted }),
					},
				};
				arrayFilters = [{ 's._id': _id }];
			}
		} else {
			return handleErrorResponse(res, 401, { message: 'Must contain the required fields' });
		}

		const data: IUser | null = await service.update({ _id: req.params.id, 'currentInvolvement.company': req.user.involvement.company }, body, arrayFilters);
		if (!data) throw new Error(userConstants.messages.User_Not_Found);
		//RESPONSE STATEMENTS
		const response = { message: userConstants.messages.User_Updated_Successfully, data };
		handleResponse(res, 200, response);
	} catch (e) {
		next(e);
	}
};

export const remove = async (req: any, res: Response, next: NextFunction) => {
	try {
		const data: IUser | null = await service.remove({ _id: req.params.id, 'currentInvolvement.company': req.user.involvement.company });
		if (!data) throw new Error(userConstants.messages.User_Not_Found);
		//RESPONSE STATEMENTS
		const response = { message: userConstants.messages.User_Deleted_Successfully, data };
		handleResponse(res, 200, response);
	} catch (e) {
		next(e);
	}
};
