import { Router } from 'express';
import { authenticateToken } from '../../middlewares/auth';
import { authorizeRole } from '../../middlewares/checkRole';
import { create, fetchUsers, fetchUser, remove, update, search, updateAddress } from './controller';

const userRouter = Router();
userRouter.route('/create').post(authenticateToken, authorizeRole, create);
userRouter.route('/list').get(authenticateToken, fetchUsers);
userRouter.route('/search').get(authenticateToken, search);
userRouter.route('/:id').get(authenticateToken, fetchUser);

userRouter.route('/:id/update').put(authenticateToken, update);
userRouter.route('/:id/update/address').put(authenticateToken, updateAddress);
userRouter.route('/:id/delete').delete(authenticateToken, authorizeRole, remove);

export default userRouter;
