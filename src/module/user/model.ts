import { Capitalize } from '../../utils/capitalize';

import { model, Schema, Model } from 'mongoose';
import { toLowerCase } from '../../utils/string';
import { hashPassword } from '../auth/service';
import { IUser } from './interface';
import { commonConstants } from '../../common/constant';


const UserSchema: Schema = new Schema(
	{
		name: {
			type: String,
			required: [true, 'Please Enter Your Name'],
		},
		username: {
			type: String,
			unique: true,
			sparse: true,
			required: [true, 'Please Enter Your Username'],
			lowercase: true,
			trim: true,
			set: toLowerCase,
		},
		email: {
			type: String,
			required: [true, 'Please Enter Your Email'],
			sparse: true,
			unique: true,
			trim: true,
		},
		password: {
			type: String,
			required: [true, 'Please Enter Your Password'],
			select: false,
		},
		address: {
			street: {
				type: String,
			},
			city: {
				type: String,
			},
			state: {
				type: String,
			},
			zip: {
				type: String,
			},
			country: {
				type: String,
			}
		},
		phone_number: {
			type: String,
		},
		gender: {
			type: String,
		},
		company: {
			companyId: {
				type: Schema.Types.ObjectId,
				ref: 'companies',
			},
			role: {
				type: String,
				enum: [commonConstants.roles.SUPER_ADMIN, commonConstants.roles.ADMIN, commonConstants.roles.CUSTOMER],
			},
		},
		active: {
			type: Boolean,
			default: false,
		},
		isDeleted: {
			type: Boolean,
			default: false,
		},
	},
	{ timestamps: true }
);

UserSchema.pre('save', function (next) {
	this.name = Capitalize(this.name);
	this.password = hashPassword(this.password);
	next();
});

const UserModel: Model<IUser> = model('users', UserSchema);
export default UserModel;
