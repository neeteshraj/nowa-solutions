export const userConstants = {
  projection: {
    _id: 1, name: 1, username: 1, email: 1, involvement: 1,
    currentInvolvement: {
      company: 1,
      type: 1,
      role: 1,
      company_details: {
        _id: 1, name: 1, type: 1, active: 1, owner: 1, createdAt: 1
      }
    },
    active: 1, createdAt: 1
  },
  externalAccessType: {
    USER_REGISTRATION: 'USER_REGISTRATION'
  },
  EMAIL_REGISTRATION: 'EMAIL REGISTRATION',
  messages: {
    errors: {
      INVALID_QUERY: 'Invalid Query'
    },
    User_Already_Exists: 'User Already Exists',
    User_Login_Successfully: 'User Login Successfully',
    User_Not_Found: 'User Not Found',
    User_Not_Registered: 'User Not Registered',
    Invalid_Credentials: "InvalidCredentials",
    User_Registered_Successfully: 'User Registered Successfully',
    User_Created_Successfully: 'User Created Successfully',
    User_Deleted_Successfully: 'User Deleted Successfully',
    User_Updated_Successfully: 'User Updated Successfully',
    SWITCHED_SUCCESSFULLY: 'Switched Successfully',
  },
}