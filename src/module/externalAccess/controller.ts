
import { Request, Response, NextFunction } from 'express';
import { IExternalAccess } from './interface';
import { findById, findByEmail } from '../user/services'
import * as service from './service'
import { createToken } from '../auth/service';

export const verify = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const hash: string = req.params.hash
    const externalAccess: IExternalAccess = await service.verify(hash)
    if (externalAccess.type === 'USER_REGISTRATION') {
      const user = await findById(externalAccess.user)
      if (!user) throw new Error("User Doesn't Exists")
      const token = createToken(user)
      return res.status(301).redirect(`${process.env.CLIENT_HOST}?token=${token}`)
    }
    if (externalAccess.type === 'FORGET_PASSWORD') {
      const user = await findById(externalAccess.user)
      if (!user) throw new Error("UserDoesn'tExists")
      return res.status(301).redirect(`${process.env.CLIENT_HOST}/changePassword?hash=${externalAccess.hash}&email=${user.email}`)
    }
    res.status(301).redirect(`${process.env.CLIENT_HOST}/login`)
  } catch (e) {
    res.status(301).redirect(`${process.env.CLIENT_HOST}/link-error`)
  }

}


export const changePassword = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const hash: string = req.params.hash
    await service.verify(hash)
    res.status(301).redirect(`${process.env.CLIENT_HOST}/set-password`)
  } catch (e) {
    res.status(301).redirect(`${process.env.CLIENT_HOST}/error`)
  }

}
