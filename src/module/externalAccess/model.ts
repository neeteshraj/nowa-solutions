import { model, Schema, Model } from 'mongoose';
import { IExternalAccess } from './interface';

const ExternalAccessSchema: Schema = new Schema(
	{
		hash: { type: String },
		type: { type: String },
		user: {
			type: Schema.Types.ObjectId,
			ref: 'user',
		},
		email: {
			type: String,
		},
		active: { type: Boolean, default: true },
	},
	{ timestamps: true }
);

const ExternalAccessModel: Model<IExternalAccess> = model('externalAccesses', ExternalAccessSchema);

export default ExternalAccessModel;
