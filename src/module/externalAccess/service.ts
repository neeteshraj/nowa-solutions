import { IExternalAccess } from "./interface"
import ExternalAccessModel from "./model"
import { activate } from '../user/services'

export const findByHash: (hash: string) => Promise<IExternalAccess | null> = async (hash) => {
  return await ExternalAccessModel.findOne({ hash: hash, active: true })
}
export const findOnlyByHash: (hash: string) => Promise<IExternalAccess | null> = async (hash) => {
  return await ExternalAccessModel.findOne({ hash: hash })
}
export const findHashByUserId: (userId: string) => Promise<IExternalAccess | null> = async (userId) => {
  return await ExternalAccessModel.findOne({ user: userId, active: true })
}
export const updateExternalAccess = async (hash: string, data: any) => {
  return await ExternalAccessModel.findOneAndUpdate({ hash }, data)
}


export const verify: (hash: string) => Promise<IExternalAccess> = async (hash) => {
  const externalAccess: IExternalAccess | null = await findByHash(hash)
  if (!externalAccess) throw new Error('Invalid Link')
  await externalAccess.updateOne({ active: false })
  await activate(externalAccess.user)
  return externalAccess
}

export const DisableExternalAccess = async (hash: string) => {
  const externalAccess: IExternalAccess | null = await findByHash(hash)
  if (!externalAccess) throw new Error('Invalid Link')
  await externalAccess.updateOne({ active: false })
}

export const create = async (userID: string, hash: string, type: string) => {
  await new ExternalAccessModel({ user: userID, hash: hash, type: type }).save()
}
export const createInvitedAccess = async (email: string, hash: string, type: string, projectId: string) => {
  await new ExternalAccessModel({ email: email, hash: hash, type: type, project: projectId }).save()
}

export const invalidPreviousLink = async (userID: string) => {
  const externalAccess = await ExternalAccessModel.find({ user: userID })
  if (externalAccess) {
    externalAccess.forEach(async (access) => {
      await access.updateOne({ active: false })
    })
  }
  return externalAccess[0]

}
