import { Document } from 'mongoose';
export interface IExternalAccess extends Document {
  hash: string
  type: string
  user: string
  email: string
  active: boolean
}
