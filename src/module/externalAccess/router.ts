import { Router } from 'express'
import { verify, changePassword } from './controller'
const externalAccessRouter = Router()

externalAccessRouter.route('/verify/:hash').post(verify)
externalAccessRouter.route('/:hash/changePassword').post(changePassword)

export default externalAccessRouter
