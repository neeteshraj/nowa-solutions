export const companyConstants = {
	messages: {
		Company_Created_Successfully: 'Company Created Successfully',
		Company_Not_Found: 'Company Not Found',
		Company_Updated_Successfully: 'Company Updated Successfully',
		Company_Deleted_Successfully: 'Company Deleted Successfully',
	},
	type: {
		RECRUITER: 'RECRUITER',
		AGENCY: 'AGENCY',
	},
};
