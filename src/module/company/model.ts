import { companyConstants } from './constant';
import { Capitalize } from '../../utils/capitalize';
import { model, Schema, Model } from 'mongoose';
import { ICompany } from './interface';

const companySchema: Schema = new Schema(
	{
		name: {
			type: String,
			required: [true, 'Name must be provided'],
		},
		type: {
			type: String,
			enum: [companyConstants.type.RECRUITER, companyConstants.type.AGENCY],
			default: companyConstants.type.AGENCY,
		},
		// involved_party: [
		// 	{
		// 		type: Schema.Types.ObjectId,
		// 		ref: 'companies',
		// 	},
		// ],
		owner: {
			type: Schema.Types.ObjectId,
			ref: 'users',
		},
		address: {
			street: {
				type: String,
			},
			city: {
				type: String,
			},
			state: {
				type: String,
			},
			zip: {
				type: String,
			},
			country: {
				type: String,
			},
		},
		isDeleted: {
			type: Boolean,
			default: false,
		},
	},
	{ timestamps: true }
);

companySchema.pre('save', function (next) {
	this.name = Capitalize(this.name);
	next();
});

const CompanyModel: Model<ICompany> = model('companies', companySchema);
export default CompanyModel;
