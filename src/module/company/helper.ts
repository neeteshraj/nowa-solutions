import { Capitalize } from '../../utils/capitalize';
import { ICompany } from './interface';

export const mapCompany = (org: ICompany) => {
  if (org.name) org.name = Capitalize(org.name)
  return org
}