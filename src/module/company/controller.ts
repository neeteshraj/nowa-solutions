import { userConstants } from './../user/constant';
import { Response, NextFunction } from 'express';
import { commonConstants } from '../../common/constant';
import { handleResponse } from '../../common/response';
import { companyConstants } from './constant';
import { mapCompany } from './helper';
import { ICompany } from './interface';
import * as service from './services';
import { update as updateUser } from '../user/services';
import { IInvolvementCompany } from '../user/interface';
import { createToken } from '../auth/service';
import { IUserRequest } from '../../common/interface';

export const create = async (req: IUserRequest, res: Response, next: NextFunction) => {
	try {
		req.body.owner = req.user._id;
		const company: ICompany = await service.create(req.body);
		const involvement: IInvolvementCompany = { companyId: company._id, role: 'SUPER_ADMIN' };

		const user = await updateUser({ _id: req.user._id }, { $set: { company: involvement } });
		if (!user) throw new Error(userConstants.messages.User_Not_Found);
		const token = createToken(user);

		const data = {
			token,
			company,
		};
		const response = { message: companyConstants.messages.Company_Created_Successfully, data };

		handleResponse(res, 201, response);
		res.status(201).json();
	} catch (e) {
		next(e);
	}
};

export const search = async (req: IUserRequest, res: Response, next: NextFunction) => {
	const keyword = req.query.keyword;
	//If keyword is not defined then return Invalid query
	if (!keyword) return next(new Error(commonConstants.messages.errors.INVALID_QUERY));

	try {
		const data = await service.search(keyword);

		handleResponse(res, 201, { data });
	} catch (e) {
		next(e);
	}
};

export const fetchCompanies = async (req: IUserRequest, res: Response, next: NextFunction) => {
	const { offset, limit, type } = req.query;
	const inactive = !!req.query.inactive;
	try {
		const condition = {
			//if inactive is false then it should return all active data only.
			...(!inactive && { active: true }),
			...(type && { type: type.toString().toUpperCase() }),
		};
		const response = await service.list(condition, Number(limit ?? 20), Number(offset ?? 0));
		handleResponse(res, 201, response);
	} catch (e) {
		next(e);
	}
};

export const fetchCompany = async (req: IUserRequest, res: Response, next: NextFunction) => {
	try {
		const data: ICompany | null = await service.findOne(req.params.id);
		if (!data) throw new Error(companyConstants.messages.Company_Not_Found);
		const response = { data };
		handleResponse(res, 200, response);
	} catch (e) {
		next(e);
	}
};

export const update = async (req: IUserRequest, res: Response, next: NextFunction) => {
	try {
		const data: ICompany | null = await service.update(req.params.id, mapCompany(req.body));
		if (!data) throw new Error(companyConstants.messages.Company_Not_Found);
		//RESPONSE STATEMENTS
		const response = { message: companyConstants.messages.Company_Updated_Successfully, data };
		handleResponse(res, 200, response);
	} catch (e) {
		next(e);
	}
};

export const remove = async (req: IUserRequest, res: Response, next: NextFunction) => {
	try {
		const data: ICompany | null = await service.remove(req.params.id);
		if (!data) throw new Error(companyConstants.messages.Company_Not_Found);
		//RESPONSE STATEMENTS
		const response = { message: companyConstants.messages.Company_Deleted_Successfully, data };
		handleResponse(res, 200, response);
	} catch (e) {
		next(e);
	}
};
