import { IAddress } from '../user/interface';

export interface ICompany {
	_id: string;
	name: string;
	type: string;
	involved_party?: string[];
	owner: string;
	isDeleted: boolean;
	address: IAddress;
}
