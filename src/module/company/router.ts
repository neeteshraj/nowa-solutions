import { Router } from 'express';
import { authenticateToken } from '../../middlewares/auth';
import { authorizeRole } from '../../middlewares/checkRole';
import { create, fetchCompanies, search, fetchCompany, update, remove } from './controller';

const router = Router();
router.route('/create').post(authenticateToken, create);
router.route('/list').get(authenticateToken, fetchCompanies);
router.route('/search').get(authenticateToken, search);
router.route('/:id').get(authenticateToken, fetchCompany);

router.route('/:id/update').put(authenticateToken, authorizeRole, update);
router.route('/:id/delete').delete(authenticateToken, authorizeRole, remove);

export default router;
