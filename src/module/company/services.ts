import Model from './model';
import { ICompany } from './interface';
import { commonConstants } from '../../common/constant';

export const create = async (org: ICompany): Promise<ICompany> => {
	return await Model.create(org);
};

export const activate = async (id: string): Promise<void> => {
	await Model.updateOne({ _id: id }, { $set: { active: true } });
};

export const list = async (condition: any, limit: number, skip: number): Promise<{ totalCount: number; data: ICompany[] }> => {
	return new Promise((resolve, reject) => {
		Model.aggregate([
			{
				$facet: {
					data: [
						{ $match: { ...condition, isDeleted: false } },
						{ $sort: { name: 1 } },
						{ $limit: limit ?? 20 },
						{ $skip: skip ?? 0 },
						// { $project: userConstants.projection }
					],
					total: [{ $match: { ...condition, isDeleted: false } }, { $count: 'totalCount' }],
				},
			},
		])
			.then(([{ data, total }]) => {
				let totalCount: number;
				total.length > 0 ? (totalCount = total[0].totalCount) : (totalCount = 0);
				return resolve({ totalCount, data });
			})
			.catch((e) => reject({ msg: commonConstants.messages.errors.LISTING_ERROR }));
	});
};

export const remove = async (_id: string): Promise<any> => {
	return await Model.findOneAndUpdate({ _id, isDeleted: false }, { $set: { isDeleted: true } }, { new: true, projection: { _id: 1 } });
};

export const update = async (_id: string, data: ICompany): Promise<ICompany | null> => await Model.findOneAndUpdate({ _id, isDeleted: false }, { $set: data }, { new: true });

export const findOne = async (_id: string): Promise<ICompany | null> => await Model.findOne({ _id, isDeleted: false }).populate('owner');

export const search = async (keyword: any) => {
	const regExp = keyword
		.trim()
		.replace(/\s\s+/g, ' ')
		.split(' ')
		.map((item: any) => new RegExp(item, 'i'));
	const data = await Model.aggregate([
		{
			$match: {
				$or: [{ name: { $in: regExp } }],
			},
		},
	]);
	return data;
	// return await UserModel.find({ name: { $regex: condition, $options: '$i' } });
};
