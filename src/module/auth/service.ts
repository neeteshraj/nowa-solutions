import { authConstants } from './constant';
import { userConstants } from './../user/constant';
import { IUser } from '../user/interface';
import { ILogin } from './interface';
import { genSaltSync, hashSync, compareSync } from 'bcrypt';
import { sign } from 'jsonwebtoken';
import UserModel from '../user/model';

export const createToken = (data: IUser): string => {
	const secret: string | undefined = process.env.ACCESS_TOKEN_SECRET;
	if (!secret) throw new Error(authConstants.messages.Missing_Secret_Data);
	const info = { username: data.username, _id: data._id, company: data.company.companyId };
	const token = sign(info, secret, { expiresIn: process.env.JWT_EXPIRE_DATE });
	return token;
};

export const hashPassword = (password: string): string => {
	const salt = genSaltSync(authConstants.saltRounds);
	const hash = hashSync(password, salt);
	return hash;
};

export const login = async (data: ILogin) => {
	return await UserModel.findOne({ $or: [{ username: data.email ?? data.username }, { email: data.email ?? data.username }] })
		.select('+password')
		.then((user: IUser | null) => {
			if (!user) throw new Error(userConstants.messages.User_Not_Found);
			if (!user.active) throw new Error(userConstants.messages.User_Not_Registered);
			const result: boolean = compareSync(data.password, user.password);
			if (!result) throw new Error(userConstants.messages.Invalid_Credentials);
			const token = createToken(user);
			return {
				username: user.username,
				email: user.email,
				_id: user._id,
				token,
			};
		});
};
