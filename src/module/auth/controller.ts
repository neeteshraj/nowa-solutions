import { checkExistingEmailOrUsername, create } from './../user/services';
import { Request, Response, NextFunction } from 'express';
import { IUser } from '../user/interface';
import { v4 as uuidv4 } from 'uuid';
import { registrationEmailHtml } from '../../common/mailHtml';
import { handleResponse } from '../../common/response';
import { userConstants } from '../user/constant';
import { create as createExternalAccessService } from '../externalAccess/service';
import sendEmail from '../../common/mailtrap';
import * as service from './service';

export const register = async (req: Request, res: Response, next: NextFunction) => {
	try {
		//Check For Existing User By Email or username
		const oldUser: IUser | null = await checkExistingEmailOrUsername(req.body.username, req.body.email);
		//Throws Error if User is Already Exists
		if (oldUser) throw new Error(userConstants.messages.User_Already_Exists);
		//Create New User
		const user = await create(req.body);
		const hash = uuidv4();
		await createExternalAccessService(user._id, hash, userConstants.externalAccessType.USER_REGISTRATION);
		sendEmail(user.email, userConstants.EMAIL_REGISTRATION, registrationEmailHtml(user.name, hash));
		const response = { username: user.username, email: user.email, message: userConstants.messages.User_Registered_Successfully };
		handleResponse(res, 201, response);
	} catch (e) {
		next(e);
	}
};

export const login = async (req: Request, res: Response, next: NextFunction) => {
	try {
		const data = await service.login(req.body);
		const response = { message: userConstants.messages.User_Login_Successfully, data };
		handleResponse(res, 200, response);
	} catch (e) {
		next(e);
	}
};
